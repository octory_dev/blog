package ru.afanasev.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.afanasev.domain.enums.ModerationStatus;
import ru.afanasev.domain.models.Post;

import java.time.LocalDateTime;
import java.util.List;

public interface PostRepository extends JpaRepository<Post, Integer> {

    /**
     * Получение количества постов с определенным статусом и активностью
     * @param moderationStatus статус модерации
     * @param isActive активный или нет пост
     */
    int countByModerationStatusAndIsActive(ModerationStatus moderationStatus, boolean isActive);

    /**
     * Получение страницы постов с определенным статусом и активностью созданные до указанной даты
     * @param moderationStatus статус модерации
     * @param isActive активный или нет пост
     * @param time дата создания
     * @param page пагинция
     */
    List<Post> findByIsActiveAndModerationStatusAndTimeBefore(boolean isActive, ModerationStatus moderationStatus, LocalDateTime time, Pageable page);
}
