package ru.afanasev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.afanasev.domain.models.Vote;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote, Integer> {

}
