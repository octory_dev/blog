package ru.afanasev.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.afanasev.domain.models.Captcha;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface CaptchaRepository extends JpaRepository<Captcha, Integer> {

    /**
     * Получение каптчи по секретному коду
     * @param secretCode секретный код
     */
    Optional<Captcha>findBySecretCode(String secretCode);

    /**
     * Удаление старой каптчи
     * @param time срок жизни каптчи
     */
    void deleteByTimeBefore(LocalDateTime time);
}
