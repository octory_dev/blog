package ru.afanasev.mapper;


import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.dto.UserDto;
import ru.afanasev.domain.models.User;
import ru.afanasev.service.PostService;
import ru.afanasev.utils.StringUtils;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class UserMapper {

    private final PostService postService;

    public UserDto map(User user) {

        int moderationCount = user.isModerator() ? postService.countModerationPosts() : 0;
        return UserDto.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .moderation(user.isModerator())
                .settings(user.isModerator())
                .moderationCount(moderationCount)
                .build();
    }

    public User map(Auth.RegisterDto registerDto) {
        return User.builder()
                .email(registerDto.getEmail())
                .password(StringUtils.hashText(registerDto.getPassword()))
                .name(registerDto.getName())
                .regTime(LocalDateTime.now())
                .build();
    }
}
