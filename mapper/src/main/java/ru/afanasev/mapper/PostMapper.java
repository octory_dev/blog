package ru.afanasev.mapper;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.afanasev.domain.dto.Post;
import ru.afanasev.domain.enums.ModerationStatus;
import ru.afanasev.domain.models.User;
import ru.afanasev.domain.models.Vote;
import ru.afanasev.service.TagService;
import ru.afanasev.utils.TimeUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class PostMapper {

    private final TagService tagService;

    public List<Post.PostResponseDto> map(List<ru.afanasev.domain.models.Post> posts) {
        return posts.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }

    public Post.PostResponseDto map(ru.afanasev.domain.models.Post post) {

        Map<Boolean, Long> partition = post.getVotes().stream()
                .collect(Collectors.groupingBy(Vote::getValue, Collectors.counting()));

        return Post.PostResponseDto.builder()
                .id(post.getId())
                .likeCount(partition.get(true))
                .dislikeCount(partition.get(false))
                .build();
    }

    public ru.afanasev.domain.models.Post map(Post.PostRequestDto dto, User user) {
        return ru.afanasev.domain.models.Post.builder()
                .moderationStatus(ModerationStatus.NEW)
                .isActive(dto.getActive() == 1)
                .user(user)
                .tags(tagService.findTagsByName(dto.getTags()))
                .time(TimeUtils.getTime(dto.getTimestamp()))
                .build();
    }
}
