package ru.afanasev.mapper;

import ru.afanasev.domain.enums.SortField;

import java.beans.PropertyEditorSupport;

public class SortFieldConverter  extends PropertyEditorSupport {

    @Override
    public void setAsText(final String mode) throws IllegalArgumentException {
        this.setValue(SortField.map(mode));
    }
}
