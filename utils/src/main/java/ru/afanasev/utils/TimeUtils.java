package ru.afanasev.utils;

import lombok.experimental.UtilityClass;

import java.sql.Timestamp;
import java.time.LocalDateTime;

@UtilityClass
public class TimeUtils {

    public LocalDateTime getTime(Long timestamp) {
        return new Timestamp(timestamp).toLocalDateTime();
    }
}
