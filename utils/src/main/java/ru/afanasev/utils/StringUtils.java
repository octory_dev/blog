package ru.afanasev.utils;

import lombok.experimental.UtilityClass;


import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Random;

@UtilityClass
public class StringUtils {

    public String hashText(String text) {
        MessageDigest messageDigest;
        byte[] digest = new byte[0];

        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(text.getBytes());
            digest = messageDigest.digest();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        BigInteger bigInt = new BigInteger(1, digest);
        StringBuilder md5Hex = new StringBuilder(bigInt.toString(16));

        while( md5Hex.length() < 32 ){
            md5Hex.insert(0, "0");
        }

        return md5Hex.toString();
    }

    public String generateSecretCode() {
        var timestamp = Long.toString(new Date().getTime());
        return hashText(timestamp);
    }

    public String generateCode(){
        var random = new Random();
        return String.format("%04d", random.nextInt(10000));
    }

}
