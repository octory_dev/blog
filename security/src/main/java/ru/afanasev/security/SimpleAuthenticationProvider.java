package ru.afanasev.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.enums.Role;
import ru.afanasev.domain.exceptions.NotFoundException;
import ru.afanasev.domain.exceptions.WrongPasswordException;
import ru.afanasev.domain.models.User;
import ru.afanasev.repository.UserRepository;
import ru.afanasev.utils.StringUtils;

import java.util.Collections;


@Slf4j
@Component
@RequiredArgsConstructor
public class SimpleAuthenticationProvider implements AuthenticationProvider {

    private final UserRepository userRepository;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        var email = authentication.getName();
        var user = userRepository.findByEmail(email).orElseThrow(NotFoundException::new);
        var role = user.isModerator() ? Role.ROLE_MODERATOR : Role.ROLE_USER;
        if (user.getPassword().equals(StringUtils.hashText(authentication.getCredentials().toString())))
            return new UsernamePasswordAuthenticationToken(
                    user, user.getPassword(), Collections.singleton(new SimpleGrantedAuthority(role.name())));
        else throw new WrongPasswordException();
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
