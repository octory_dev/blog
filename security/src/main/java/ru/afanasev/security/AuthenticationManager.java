package ru.afanasev.security;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.models.User;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthenticationManager {

    private final SimpleAuthenticationProvider authenticationProvider;

    public User authenticate(Auth.LoginDto form) {
        var auth = authenticationProvider.authenticate(
                new UsernamePasswordAuthenticationToken(form.getEmail(), form.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(auth);
        return (User) auth.getPrincipal();
    }

    public User getAuthenticatedUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication instanceof AnonymousAuthenticationToken ? null : (User) authentication.getPrincipal();
    }
}
