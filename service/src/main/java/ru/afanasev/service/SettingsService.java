package ru.afanasev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.enums.SystemCode;
import ru.afanasev.domain.models.GlobalSettings;
import ru.afanasev.repository.GlobalSettingsRepository;
import ru.afanasev.service.config.InitProperties;

import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SettingsService {

    private final InitProperties initProperties;
    private final GlobalSettingsRepository globalSettingsRepository;

    public InitProperties findInitProperties() {
        return initProperties;
    }

    public Map<SystemCode, Boolean> findGlobalSettings() {
        return globalSettingsRepository.findAll().stream()
                .collect(Collectors.toMap(GlobalSettings::getCode, GlobalSettings::isValue));
    }
}
