package ru.afanasev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.models.Tag;
import ru.afanasev.repository.TagRepository;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TagService {

    private final TagRepository tagRepository;

    public Set<Tag> findTagsByName(String[] tags) {
        var entries = tagRepository.findByNameIn(tags).stream()
                .collect(Collectors.toMap(Tag::getName, t -> t));
        Arrays.stream(tags).forEach(tag -> {
            if (!entries.containsKey(tag)) {
                entries.put(tag, Tag.builder()
                        .name(tag)
                        .time(LocalDateTime.now())
                        .build());
            }
        });
        return new HashSet<>(entries.values());
    }
}
