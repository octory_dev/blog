package ru.afanasev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.exceptions.NotFoundException;
import ru.afanasev.domain.models.User;
import ru.afanasev.repository.UserRepository;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(NotFoundException::new);
    }

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public boolean existsByEmail(String email) {
        return  userRepository.existsByEmail(email);
    }
}
