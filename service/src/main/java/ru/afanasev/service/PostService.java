package ru.afanasev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.enums.ModerationStatus;
import ru.afanasev.domain.enums.SortField;
import ru.afanasev.domain.models.Post;
import ru.afanasev.repository.PostRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PostService {

    private final PostRepository postRepository;

    public int countModerationPosts() {
        return postRepository.countByModerationStatusAndIsActive(ModerationStatus.NEW, false);
    }

    public List<Post> findActivePosts(Integer offset, Integer limit, SortField sortField) {
        return postRepository.findByIsActiveAndModerationStatusAndTimeBefore(
                true, ModerationStatus.ACCEPTED, LocalDateTime.now(),
                PageRequest.of(offset, limit, sortField.getDirection(), sortField.getSortField()));
    }

    public Post addPost(Post post) {
        return postRepository.save(post);
    }

}
