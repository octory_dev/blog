package ru.afanasev.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.models.User;
import ru.afanasev.security.AuthenticationManager;
import ru.afanasev.security.SimpleAuthenticationProvider;

import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthenticationService {

    private final UserService userService;
    private final CaptchaService captchaService;
    private final AuthenticationManager authenticationManager;


    public User authenticateUser(Auth.LoginDto form) {
        return authenticationManager.authenticate(form);
    }

    public void register(User user) {
        userService.updateUser(user);
    }

    public Map<String, String> getCaptcha(){
        var generatedCaptcha = captchaService.generateCaptcha();
        var image = captchaService.generateImage(generatedCaptcha.getCode());
        return Map.of(generatedCaptcha.getSecretCode(), image);
    }

    public User getAuthenticatedUser() {
        return authenticationManager.getAuthenticatedUser();
    }
}
