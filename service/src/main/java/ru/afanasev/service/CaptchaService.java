package ru.afanasev.service;

import com.github.cage.Cage;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.afanasev.domain.models.Captcha;
import ru.afanasev.repository.CaptchaRepository;
import ru.afanasev.utils.StringUtils;

import java.io.*;
import java.time.LocalDateTime;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class CaptchaService {

    private final static String IMAGE_PREFIX = "data:image/png;base64, ";

    private final Cage cage;
    private final CaptchaRepository captchaRepository;

    public String generateImage(String code) {
        try(ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            cage.draw(code, os);

            byte[] imageBytes = os.toByteArray();
            Base64.Encoder encoder = Base64.getEncoder();
            return IMAGE_PREFIX + encoder.encodeToString(imageBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Captcha generateCaptcha() {
        return captchaRepository.save(Captcha.builder()
                .code(StringUtils.generateCode())
                .time(LocalDateTime.now())
                .secretCode(StringUtils.generateSecretCode())
                .build());
    }
}
