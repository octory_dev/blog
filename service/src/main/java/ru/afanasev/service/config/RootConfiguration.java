package ru.afanasev.service.config;

import com.github.cage.Cage;
import com.github.cage.GCage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RootConfiguration {

    @Bean
    public Cage getCage() {
        return new GCage();
    }
}
