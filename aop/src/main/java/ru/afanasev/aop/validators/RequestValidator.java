package ru.afanasev.aop.validators;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.afanasev.aop.annotations.RequestValid;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.dto.Post;
import ru.afanasev.repository.CaptchaRepository;
import ru.afanasev.repository.UserRepository;
import ru.afanasev.utils.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@Slf4j
@RequiredArgsConstructor
public class RequestValidator implements ConstraintValidator<RequestValid, Object> {

    private final static String EMAIL_LABEL = "email";
    private final static String EMAIL_EXIST = "Этот e-mail уже зарегистрирован";
    private final static String CAPTCHA_LABEL = "captcha";
    private final static String INVALID_CAPTCHA = "Код с картинки введён неверно";
    private final static String PASSWORD_LABEL = "password";
    private final static String INCORRECT_PASSWORD = "Пароль короче 6-ти символов";
    private final static String TEXT_LABEL = "text";
    private final static String TEXT_EMPTY = "Текст публикации не установлен";
    private final static String TEXT_SMALL = "Текст публикации слишком короткий";
    private final static String TITLE_LABEL = "title";
    private final static String TITLE_EMPTY = "Заголовок не установлен";
    private final static String TITLE_SMALL = "Заголовок слишком короткий";


    private final UserRepository userRepository;
    private final CaptchaRepository captchaRepository;

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext context) {
        boolean isValid;
        if (Post.PostRequestDto.class.equals(o.getClass())) {
            isValid = this.postValidate(context, (Post.PostRequestDto) o);
        } else if(Auth.RegisterDto.class.equals(o.getClass())) {
            isValid = this.registerValidate(context, (Auth.RegisterDto) o);
        } else {
            isValid = true;
        }

        return isValid;
    }

    private boolean registerValidate(ConstraintValidatorContext context, Auth.RegisterDto value) {

        context.disableDefaultConstraintViolation();
        var isValid = true;

        if (userRepository.existsByEmail(value.getEmail())) {
            isValid = false;
            addCustomMessageForValidation(context, EMAIL_LABEL, EMAIL_EXIST);
        }
        if (value.getPassword().length() < 6) {
            isValid = false;
            addCustomMessageForValidation(context, PASSWORD_LABEL, INCORRECT_PASSWORD);
        }
        var captcha = captchaRepository.findBySecretCode(value.getCaptchaSecret())
                .orElse(null);
        if (captcha == null || !captcha.getCode().equals(value.getCaptcha())) {
            isValid = false;
            addCustomMessageForValidation(context, CAPTCHA_LABEL, INVALID_CAPTCHA);
        }
        log.info("registerValidate({}) -> {}", value.getEmail(), isValid);
        return isValid;
    }

    private boolean postValidate(ConstraintValidatorContext context, Post.PostRequestDto value) {

        context.disableDefaultConstraintViolation();
        var isValid = true;

        if (value.getTitle() == null || value.getTitle().length() == 0) {
            isValid = false;
            addCustomMessageForValidation(context, TITLE_LABEL, TITLE_EMPTY);
        } else if (value.getTitle().length() < 4) {
            isValid = false;
            addCustomMessageForValidation(context, TITLE_LABEL, TITLE_SMALL);
        }
        if (value.getText() == null || value.getText().length() == 0) {
            isValid = false;
            addCustomMessageForValidation(context, TEXT_LABEL, TEXT_EMPTY);
        } else if (value.getTitle().length() < 51) {
            isValid = false;
            addCustomMessageForValidation(context, TEXT_LABEL, TEXT_SMALL);
        }
        log.info("postValidate({}) -> {}", value.getTitle(), isValid);
        return isValid;
    }

    private void addCustomMessageForValidation(ConstraintValidatorContext context, String label, String message) {
        context.buildConstraintViolationWithTemplate(message).addPropertyNode(label)
                .addConstraintViolation();
    }
}
