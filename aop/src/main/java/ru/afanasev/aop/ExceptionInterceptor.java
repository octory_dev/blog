package ru.afanasev.aop;

import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.afanasev.domain.dto.ErrorDto;
import ru.afanasev.domain.exceptions.WrongPasswordException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashMap;

@Slf4j
@ControllerAdvice
public class ExceptionInterceptor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ConstraintViolationException.class)
    public final ResponseEntity<ErrorDto>handleValidatorException(ConstraintViolationException e) {

        var error = ErrorDto.builder()
                .result(false)
                .errors(new HashMap<>())
                .build();

        for (ConstraintViolation<?> constraintViolation : e.getConstraintViolations()) {
            var label =  ((PathImpl)constraintViolation.getPropertyPath()).getLeafNode().getName();
            var message = constraintViolation.getMessageTemplate();
            error.getErrors().put(label, message);
        }

        return new ResponseEntity<>(error, HttpStatus.OK);
    }

    @ExceptionHandler(WrongPasswordException.class)
    public final ResponseEntity<ErrorDto> handleWrongPasswordException(WrongPasswordException e){
        var error = ErrorDto.builder().result(false).build();
        return new ResponseEntity<>(error, HttpStatus.OK);
    }
}
