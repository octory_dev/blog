package ru.afanasev.aop.annotations;

import ru.afanasev.aop.validators.RequestValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target(PARAMETER)
@Retention(RUNTIME)
@Constraint(validatedBy = RequestValidator.class)
@Documented
public @interface RequestValid {
    String message() default "login invalid";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
