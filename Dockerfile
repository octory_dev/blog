FROM openjdk:16.0.1-jdk-buster
ARG JAR_FILE=application/build/*.jar
COPY ${JAR_FILE} application.jar
ENTRYPOINT ["java", "-jar", "/application.jar"]