package ru.afanasev.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ru.afanasev.aop.annotations.RequestValid;
import ru.afanasev.domain.dto.Post;
import ru.afanasev.domain.enums.SortField;
import ru.afanasev.domain.models.User;
import ru.afanasev.mapper.PostMapper;
import ru.afanasev.mapper.SortFieldConverter;
import ru.afanasev.service.PostService;
import ru.afanasev.service.UserService;

import java.security.Principal;
import java.util.concurrent.CountDownLatch;

@RestController
@Slf4j
@Validated
@RequestMapping("api/post")
@RequiredArgsConstructor
public class PostController {

    private final PostService postService;
    private final PostMapper postMapper;
    private final UserService userService;
    private final ThreadPoolTaskExecutor executor;

    @GetMapping
    public Post getPosts(@RequestParam(value = "mode", required = false, defaultValue = "recent") SortField sortField,
                         @RequestParam(required = false, defaultValue = "0") int offset,
                         @RequestParam(required = false, defaultValue = "15") int limit) {

        final var watch = new CountDownLatch(2);
        final var page = new Post();

        executor.execute(() -> {
            page.setPosts(postMapper.map(postService.findActivePosts(offset, limit, sortField)));
            watch.countDown();
        });
        executor.execute(() -> {
            page.setCount(postService.countModerationPosts());
            watch.countDown();
        });

        try {
            watch.await();
        } catch (InterruptedException e) {
            log.error("Count down watch interruption", e);
        }

        return page;
    }

    @PostMapping
    public Post.ResultDto addPost(@RequestBody @RequestValid Post.PostRequestDto dto, Principal principal) {
        postService.addPost(postMapper.map(dto, (User) principal));
        return Post.ResultDto.builder()
                .result(true)
                .build();
    }

    @InitBinder("mode")
    public void initBinder(WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(SortField.class, new SortFieldConverter());
    }
}
