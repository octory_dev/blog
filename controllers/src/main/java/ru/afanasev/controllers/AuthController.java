package ru.afanasev.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.afanasev.aop.annotations.RequestValid;
import ru.afanasev.domain.dto.Auth;
import ru.afanasev.domain.dto.CaptchaDto;
import ru.afanasev.domain.dto.UserDto;
import ru.afanasev.domain.exceptions.NotFoundException;
import ru.afanasev.domain.models.User;
import ru.afanasev.mapper.UserMapper;
import ru.afanasev.service.AuthenticationService;
import ru.afanasev.service.UserService;

import java.security.Principal;


@Slf4j
@RestController
@RequiredArgsConstructor
@Validated
@RequestMapping("api/auth")
public class AuthController {

    private final AuthenticationService authenticationService;
    private final UserService userService;
    private final UserMapper userMapper;

    @PostMapping("login")
    public Auth.ResultDto authenticate(@RequestBody Auth.LoginDto form) {
        try {
            var user = authenticationService.authenticateUser(form);
            return Auth.ResultDto.builder()
                    .result(true)
                    .userDto(userMapper.map(user))
                    .build();
        } catch (NotFoundException e) {
            log.info("authenticate({})", form.getEmail(), e);
            return Auth.ResultDto.builder()
                    .result(false)
                    .build();
        }
    }

    @PostMapping("register")
    public Auth.ResultDto register(@RequestBody @RequestValid Auth.RegisterDto form) {
        authenticationService.register(userMapper.map(form));
        return Auth.ResultDto.builder()
                .result(true)
                .build();
    }

    @GetMapping("captcha")
    public CaptchaDto getCaptcha() {
        var captcha = authenticationService.getCaptcha();
        var captchaDto = new CaptchaDto();
        captcha.forEach((code, image) -> {
            captchaDto.setImage(image);
            captchaDto.setSecret(code);
        });
        return captchaDto;
    }

    @GetMapping("check")
    public Auth.ResultDto check(){
        var user = authenticationService.getAuthenticatedUser();
        if (user != null) {
            return Auth.ResultDto.builder()
                    .result(true)
                    .userDto(new UserDto())
                    .build();
        } else {
            return Auth.ResultDto.builder()
                    .result(false)
                    .build();
        }
    }
}
