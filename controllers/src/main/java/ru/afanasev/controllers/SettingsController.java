package ru.afanasev.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.afanasev.domain.enums.SystemCode;
import ru.afanasev.service.SettingsService;
import ru.afanasev.service.config.InitProperties;

import java.util.Map;

@RestController
@RequestMapping("api")
@RequiredArgsConstructor
public class SettingsController {

    private final SettingsService settingsService;

    @GetMapping("init")
    public InitProperties findInitProperties() {
        return settingsService.findInitProperties();
    }

    @GetMapping("settings")
    public Map<SystemCode, Boolean> findGlobalSettings() {
        return settingsService.findGlobalSettings();
    }
}
