package ru.afanasev.domain.enums;

public enum SystemCode {
    MULTIUSER_MODE,
    POST_PREMODERATION,
    STATISTICS_IS_PUBLIC
}
