package ru.afanasev.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.data.domain.Sort;
import ru.afanasev.domain.exceptions.NotFoundException;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum SortField {
    RECENT("recent", "time", Sort.Direction.DESC),
    POPULAR("popular", "comments", Sort.Direction.DESC),
    BEST("best", "votes", Sort.Direction.DESC),
    EARLY("early", "time", Sort.Direction.ASC);


    private final String mode;
    private final String sortField;
    private final Sort.Direction direction;

    public static SortField map(String mode) {
        return Arrays.stream(SortField.values())
                .filter(sf -> sf.mode.equals(mode))
                .findFirst()
                .orElseThrow(NotFoundException::new);
    }
}
