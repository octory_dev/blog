package ru.afanasev.domain.enums;

public enum Role {
    ROLE_USER,
    ROLE_MODERATOR
}
