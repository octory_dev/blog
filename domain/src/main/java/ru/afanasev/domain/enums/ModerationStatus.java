package ru.afanasev.domain.enums;

public enum ModerationStatus {
    NEW,
    ACCEPTED
}
