package ru.afanasev.domain.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

public class Auth {

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LoginDto {
        @JsonProperty("e_mail")
        private String email;
        private String password;
    }
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class RegisterDto {
        @JsonProperty("e_mail")
        private String email;
        private String password;
        private String name;
        private String captcha;
        @JsonProperty("captcha_secret")
        private String captchaSecret;
    }
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResultDto {
        private Boolean result;
        private UserDto userDto;
    }
}
