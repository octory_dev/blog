package ru.afanasev.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Post {

    private Integer count;
    private List<PostResponseDto> posts;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PostResponseDto {
        private Integer id;
        private Long timestamp;
        private UserDto user;
        private String title;
        private String announce;
        private long likeCount;
        private long dislikeCount;
        private long commentCount;
        private long viewCount;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PostRequestDto {

        private long timestamp;
        private byte active;
        private String title;
        private String[] tags;
        private String text;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ResultDto {
        private Boolean result;
    }
}
