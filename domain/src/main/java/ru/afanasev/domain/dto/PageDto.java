package ru.afanasev.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PageDto {

    private Integer pageNum;
    private Integer pageSize;
    private String sortBy;

    private Integer getPage() {
        return pageNum * pageSize;
    }
}
