package ru.afanasev.domain.models;

import lombok.*;
import ru.afanasev.domain.enums.ModerationStatus;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "is_active")
    private boolean isActive;
    private LocalDateTime time;
    @Enumerated(EnumType.STRING)
    @Column(name = "moderation_status")
    private ModerationStatus moderationStatus;
    @ManyToOne
    @JoinColumn(name = "moderator_id")
    private User moderator;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    private String title;
    private String text;
    @Column(name = "view_count")
    private Integer viewCount;
    @OneToMany(mappedBy="post")
    @Setter(AccessLevel.NONE)
    private Set<Comment> comments = new HashSet<>();
    @OneToMany(mappedBy="post")
    @Setter(AccessLevel.NONE)
    private Set<Vote> votes = new HashSet<>();
    @ManyToMany
    private Set<Tag> tags = new HashSet<>();

    public void addComment(Comment comment) {
        comments.add(comment);
        comment.setPost(this);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
        comment.setPost(null);
    }

    public void addVote(Vote vote) {
        votes.add(vote);
        vote.setPost(this);
    }

    public void removeVote(Vote vote) {
        votes.remove(vote);
        vote.setPost(null);
    }
}
