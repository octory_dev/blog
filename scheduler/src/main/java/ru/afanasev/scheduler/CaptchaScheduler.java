package ru.afanasev.scheduler;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ru.afanasev.repository.CaptchaRepository;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class CaptchaScheduler {

    private final CaptchaRepository captchaRepository;

    @Scheduled(cron = "0 0 0 30 * *")
    public void removeOldCaptcha() {
        captchaRepository.deleteByTimeBefore(LocalDateTime.now().minusHours(1));
    }
}
